import * as functions from 'firebase-functions';
import * as sgMail from '@sendgrid/mail';

sgMail.setApiKey('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');

export const contact = functions.region('europe-west1').https.onRequest(async (request, response) => {
  response.set('Access-Control-Allow-Origin', '*');
  response.set('Access-Control-Allow-Headers', 'Content-Type');
  if (request.body.email !== undefined && request.body.email !== '' && request.body.message !== undefined && request.body.message !== '') {
    const date = new Date().toDateString();
    const msg = {
      to: request.body.email,
      from: 'no-reply@avpro.lu',
      bcc: 'louis@campbell.lu',
      templateId: 'd-140fab8551ea43c2a73bd677d79f5a93',
      dynamic_template_data: {
        message: request.body.message,
        date: date
      }
    };
    try {
      await sgMail.send(msg);
    } catch (e) {
      response.send({
        'success': false,
        'message': e.message
      });
    }

    response.send({'success': true});
    return;
  }
  response.send({'success': false});
});
