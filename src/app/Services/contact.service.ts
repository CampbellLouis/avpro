import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ContactViewModel} from '../Models/ContactViewModel';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private httpClient: HttpClient) {
  }

  public postContact(contactInformation: ContactViewModel){
    console.log(contactInformation);
    return this.httpClient.post('https://europe-west1-avpro-247217.cloudfunctions.net/contact', contactInformation).subscribe(response => {
      console.log(response);
    });
  }
}
