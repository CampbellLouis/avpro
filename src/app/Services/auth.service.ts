import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../Models/User';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private afAuth: AngularFireAuth;
  private afStore: AngularFirestore;
  private router: Router;
  user$: Observable<User>;

  constructor() {
    this.user$ = this.afAuth.authState.pipe(
      switchMap((user) => {
          if (user) {
            return this.afStore.doc<User>(`users/{user.uid}`).valueChanges();
          }
        }
      ));
  }
}
