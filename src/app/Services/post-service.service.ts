import { Injectable } from '@angular/core';
import { AngularFlamelink } from 'angular-flamelink';

@Injectable({
  providedIn: 'root',
})
export class PostService {
  constructor(private flamelink: AngularFlamelink) {}
  public getPosts() {
    return this.flamelink.valueChanges({ schemaKey: 'post', populate: ['gallery'] });
  }
  public getPost(id: string) {
    return this.flamelink.valueChanges({ schemaKey: 'post', populate: ['gallery'], entryId: id });
  }
  public async getImageUrl(fileId) {
    return await this.flamelink.storage.getURL(fileId);
  }
}
