import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './Components/Home/home/home.component';
import { SinglePostComponent } from './Components/Blog/single-post/single-post.component';
import { BlogComponent } from './Components/Blog/blog/blog.component';
import { ContactComponent } from './Components/Contact/contact/contact.component';
import { AboutUsComponent } from './Components/About-Us/about-us/about-us.component';
import { NotFoundComponent } from './Components/Error/not-found/not-found.component';
import { LoginComponent } from './Components/Auth/login/login.component';
import { RegisterComponent } from './Components/Auth/register/register.component';
import { PrivacyPolicyComponent } from './Components/privacy-policy/privacy-policy.component';
import { ImpressumComponent } from './Components/impressum/impressum.component';
import { TeamComponent } from './Components/Team/team.component';

export const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'posts/:id', component: SinglePostComponent, pathMatch: 'prefix' },
  { path: 'blog', component: BlogComponent, pathMatch: 'full' },
  { path: 'contact', component: ContactComponent, pathMatch: 'full' },
  { path: 'about-us', component: AboutUsComponent, pathMatch: 'full' },
  { path: 'register', component: RegisterComponent, pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'privacy', component: PrivacyPolicyComponent },
  { path: 'impressum', component: ImpressumComponent },
  { path: 'team', component: TeamComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
