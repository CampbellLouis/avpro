import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent implements OnInit {
  title = 'Avpro';

  ngOnInit(): void {
    const firebaseConfig = {
      apiKey: 'AIzaSyB3Sruhfq5mJyXjOFOivh6IszYGltn8OIE',
      authDomain: 'avpro-247217.firebaseapp.com',
      databaseURL: 'https://avpro-247217.firebaseio.com',
      projectId: 'avpro-247217',
      storageBucket: 'avpro-247217.appspot.com',
      messagingSenderId: '259263966175',
      appId: '1:259263966175:web:e2b5d7d8f80136ec7c726a',
      measurementId: 'G-0JBZRBYW8B',
    };
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
  }
}
