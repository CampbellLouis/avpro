import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './Components/Shared/nav-menu/nav-menu.component';
import { HomeComponent } from './Components/Home/home/home.component';
import { HomeHeroComponent } from './Components/Home/home-hero/home-hero.component';
import { AboutUsHeroComponent } from './Components/Home/about-us-hero/about-us-hero.component';
import { FeaturedPostComponent } from './Components/Home/featured-post/featured-post.component';
import { OurVisionHeroComponent } from './Components/Home/our-vision-hero/our-vision-hero.component';
import { FeaturedPostsComponent } from './Components/Home/featured-posts/featured-posts.component';
import { FooterComponent } from './Components/Shared/footer/footer.component';
import { SinglePostComponent } from './Components/Blog/single-post/single-post.component';
import { BlogPostComponent } from './Components/Blog/blog-post/blog-post.component';
import { BlogComponent } from './Components/Blog/blog/blog.component';
import { BlogSidebarComponent } from './Components/Blog/blog-sidebar/blog-sidebar.component';
import { ContactComponent } from './Components/Contact/contact/contact.component';
import { AboutUsComponent } from './Components/About-Us/about-us/about-us.component';
import { BlogPostlistComponent } from './Components/Blog/blog-postlist/blog-postlist.component';
import { NotFoundComponent } from './Components/Error/not-found/not-found.component';
import { ContactMapComponent } from './Components/Contact/contact-map/contact-map.component';
import { LoginComponent } from './Components/Auth/login/login.component';
import { RegisterComponent } from './Components/Auth/register/register.component';
import { AppRoutingModule, routes } from './app-routing.module';
import { PostService } from './Services/post-service.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgmCoreModule } from '@agm/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CallToActionComponent } from './Components/Home/call-to-action/call-to-action.component';
import { PillarsComponent } from './Components/About-Us/pillars/pillars.component';
import { IntroComponent } from './Components/About-Us/intro/intro.component';
import { CallToActionFooterComponent } from './Components/Home/call-to-action-footer/call-to-action-footer.component';
import { ContactFormComponent } from './Components/Contact/contact-form/contact-form.component';
import { PrivacyPolicyComponent } from './Components/privacy-policy/privacy-policy.component';
import { ImpressumComponent } from './Components/impressum/impressum.component';
import { TeamComponent } from './Components/Team/team.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFlamelinkModule } from 'angular-flamelink';
import { AngularFireAuth, AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyB3Sruhfq5mJyXjOFOivh6IszYGltn8OIE',
  authDomain: 'avpro-247217.firebaseapp.com',
  databaseURL: 'https://avpro-247217.firebaseio.com',
  projectId: 'avpro-247217',
  storageBucket: 'avpro-247217.appspot.com',
  messagingSenderId: '259263966175',
  appId: '1:259263966175:web:e2b5d7d8f80136ec7c726a',
  measurementId: 'G-0JBZRBYW8B',
};
@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    HomeHeroComponent,
    FeaturedPostComponent,
    FeaturedPostsComponent,
    FooterComponent,
    AboutUsHeroComponent,
    OurVisionHeroComponent,
    SinglePostComponent,
    BlogComponent,
    BlogPostComponent,
    AboutUsComponent,
    ContactComponent,
    BlogSidebarComponent,
    TeamComponent,
    BlogPostlistComponent,
    NotFoundComponent,
    LoginComponent,
    RegisterComponent,
    ContactMapComponent,
    CallToActionComponent,
    PillarsComponent,
    IntroComponent,
    CallToActionFooterComponent,
    ContactFormComponent,
    PrivacyPolicyComponent,
    ImpressumComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFlamelinkModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB983gztmmCpIXDEgbaXDtyOiv9716eAQM',
    }),
    ReactiveFormsModule,
  ],
  providers: [PostService],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor() {}
}
