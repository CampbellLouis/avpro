import {Component, OnInit} from '@angular/core';
import {Post} from '../../../Models/Post';
import {PostService} from '../../../Services/post-service.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {
  public posts: Post[];

  constructor() {
  }

  ngOnInit() {
  }

}
