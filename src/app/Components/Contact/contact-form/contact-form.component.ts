import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ContactService} from '../../../Services/contact.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.sass']
})
export class ContactFormComponent implements OnInit {
  public contactForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private contactService: ContactService) {
  }

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      email: ['', [
        Validators.email,
        Validators.required
      ]],
      name: ['', [
        Validators.required
      ]],
      subject: ['', [
        Validators.required
      ]],
      phoneNumber: ['', [
        Validators.minLength(9)
      ]],
      message: ['', [
        Validators.required
      ]]
    });
  }

  public submit() {
    this.contactService.postContact(this.contactForm.value);
  }

  get name() {
    return this.contactForm.get('name');
  }

  get email() {
    return this.contactForm.get('email');
  }

  get subject() {
    return this.contactForm.get('subject');
  }

  get phoneNumber() {
    return this.contactForm.get('phoneNumber');
  }
  get message() {
    return this.contactForm.get('message');
  }
}
