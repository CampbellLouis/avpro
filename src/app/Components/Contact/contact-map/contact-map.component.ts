import { Component, OnInit } from '@angular/core';
import {MapTypeStyle} from "@agm/core";

@Component({
  selector: 'app-contact-map',
  templateUrl: './contact-map.component.html',
  styleUrls: ['./contact-map.component.sass']
})
export class ContactMapComponent implements OnInit {
  //49.608084 6.111642
  public lat: number = 49.811989;
  public lng: number = 6.422778;
  public styles= [
    {
      "featureType": "road",
      "stylers": [
        {
          "hue": "#1e7c99"
        },
        {
          "saturation": -79
        }
      ]
    },
    {
      "featureType": "poi",
      "stylers": [
        {
          "saturation": -78
        },
        {
          "hue": "#1e7c99"
        },
        {
          "lightness": -47
        },
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road.local",
      "stylers": [
        {
          "lightness": 22
        }
      ]
    },
    {
      "featureType": "landscape",
      "stylers": [
        {
          "hue": "#1e7c99"
        },
        {
          "saturation": -11
        }
      ]
    },
    {},
    {},
    {
      "featureType": "water",
      "stylers": [
        {
          "saturation": -65
        },
        {
          "hue": "#1e7c99"
        },
        {
          "lightness": 8
        }
      ]
    },
    {
      "featureType": "road.local",
      "stylers": [
        {
          "weight": 1.3
        },
        {
          "lightness": 30
        }
      ]
    },
    {
      "featureType": "transit",
      "stylers": [
        {
          "visibility": "simplified"
        },
        {
          "hue": "#1e7c99"
        },
        {
          "saturation": -16
        }
      ]
    },
    {
      "featureType": "transit.line",
      "stylers": [
        {
          "saturation": -72
        }
      ]
    },
    {}
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
