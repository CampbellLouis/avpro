import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-featured-post',
  templateUrl: './featured-post.component.html',
  styleUrls: ['./featured-post.component.sass'],
})
export class FeaturedPostComponent implements OnInit {
  @Input() public post;
  constructor() {}

  ngOnInit() {}
}
