import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OurVisionHeroComponent } from './our-vision-hero.component';

describe('OurVisionHeroComponent', () => {
  let component: OurVisionHeroComponent;
  let fixture: ComponentFixture<OurVisionHeroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OurVisionHeroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OurVisionHeroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
