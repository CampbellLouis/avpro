import { Component, OnInit } from '@angular/core';
import { Post } from '../../../Models/Post';
import { PostService } from '../../../Services/post-service.service';

@Component({
  selector: 'app-featured-posts',
  templateUrl: './featured-posts.component.html',
  styleUrls: ['./featured-posts.component.sass'],
})
export class FeaturedPostsComponent implements OnInit {
  public posts;
  constructor(private postService: PostService) {}

  ngOnInit() {
    this.postService.getPosts().subscribe(posts => (this.posts = posts));
  }
}
