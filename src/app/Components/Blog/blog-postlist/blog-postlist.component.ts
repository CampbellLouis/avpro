import { Component, OnInit } from '@angular/core';
import { Post } from '../../../Models/Post';
import { PostService } from '../../../Services/post-service.service';

@Component({
  selector: 'app-blog-postlist',
  templateUrl: './blog-postlist.component.html',
  styleUrls: ['./blog-postlist.component.sass'],
})
export class BlogPostlistComponent implements OnInit {
  ngOnInit(): void {}
}
