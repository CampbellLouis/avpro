import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogPostlistComponent } from './blog-postlist.component';

describe('BlogPostlistComponent', () => {
  let component: BlogPostlistComponent;
  let fixture: ComponentFixture<BlogPostlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogPostlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogPostlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
