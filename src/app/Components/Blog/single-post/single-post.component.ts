import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from '../../../Services/post-service.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.sass'],
})
export class SinglePostComponent implements OnInit {
  public id: number;
  public post;
  constructor(private route: ActivatedRoute, private postService: PostService) {}

  ngOnInit() {
    this.route.params
      .pipe(switchMap(param => this.postService.getPost(param.id)))
      .subscribe(post => (this.post = post));
  }
}
