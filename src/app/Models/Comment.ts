import {User} from "./User";

export class Comment {
  body : string;
  likes : number = 0;
  date : string;
  user : User;
  replies? : Comment[]
}
