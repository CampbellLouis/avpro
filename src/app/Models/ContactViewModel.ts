export interface ContactViewModel {
  email: string;
  name: string;
  subject: string;
  phoneNumber?: string;
  message: string;
}
