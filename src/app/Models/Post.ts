import {User} from './User';
import {Comment} from './Comment';

export class Post {
  postId: number;
  title: string;
  excerpt: string;
  author?: User;
  date: string;
  body: string;
  img?: string;
  likes = 0;
  comments?: Comment[];
}
