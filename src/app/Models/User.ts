export interface User {
    uid: string;
    email: string;
    admin: boolean;
    phoneNumber?: string;
    member: boolean;
    payedUntil: string;
}
